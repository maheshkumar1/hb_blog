<?php

namespace App\Http\Controllers;

use App\Companies;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::latest()->paginate(10);
  
        return view('companies.index',compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:companies|email',
            'logo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'web_site' => 'required|url',
        ]);
  
        $company = Companies::create($request->all());

        if(\Request::hasFile('logo')) {
            $file = \Request::file('logo');
            $exten = $file->getClientOriginalExtension();
            $rand = date('Ymdhms');
            $file_name = "logo_".$rand.$request->vendor_code.".".$exten;
            $file->move(public_path().'/storage/app/public/',$file_name);  
            $company->logo = $file_name;
            $company->save();
        }
   
        return redirect()->route('companies.index')
                        ->with('success','Company profile created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $company)
    {
        return view('companies.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit(Companies $company)
    {
        return view('companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companies $company)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            //'logo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'web_site' => 'required|url',
        ]);

        $company->update($request->all());
        
        if(\Request::hasFile('logo')) {
            $file = \Request::file('logo');
            $exten = $file->getClientOriginalExtension();
            $rand = date('Ymdhms');
            $file_name = "logo_".$rand.$request->vendor_code.".".$exten;
            $file->move(public_path().'/storage/app/public/',$file_name);  
            $company->logo = $file_name;
            $company->save();
        }
        
   
        return redirect()->route('companies.index')
                        ->with('success','Company profile updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companies $companies)
    {
        $companies->delete();
  
        return redirect()->route('companies.index')
                        ->with('success','Company deleted successfully');
    }
}
