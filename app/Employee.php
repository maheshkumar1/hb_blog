<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'companies_id'
    ];

    public function company() {
        return $this->hasOne('App\Companies', 'id', 'companies_id');
    }
}
