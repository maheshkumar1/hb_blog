@extends('layouts.app')
 
@section('content')
    <div class="row">

    <ul class="navbar-nav mr-auto">

        <li style="padding: 10px 10px 10px 10px;"> <a class="btn btn-secondary" href="{{ url('companies') }}">Companies</a> </li>
    </ul>

        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Employee Info</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('employees.create') }}"> <i class="fa fa-plus-circle" aria-hidden="true"></i> New Employee</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name:</th>
            <th>E-Mail:</th>
            <th>Company Name:</th>  
            <th>Phone:</th>          
            <th width="280px">Action</th>
        </tr>
        @foreach ($employees as $employee)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
            <td>{{ $employee->email }}</td>
            <td><a href="{{ route('employees.show',$employee->id) }}">{{ $employee->company->name }}</a></td>
            <td>{{ $employee->phone }}</td>
            <td>
                <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('employees.show',$employee->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
    
                    <a class="btn btn-primary" href="{{ route('employees.edit',$employee->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $employees->links() !!}
      
@endsection