@extends('layouts.app')
 
@section('content')

    <ul class="navbar-nav mr-auto">

    <li style="padding: 10px 10px 10px 10px;">  <a class="btn btn-info" href="{{ url('employees') }}">Employees</a> </li>
    </ul>


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Companies Info</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('companies.create') }}"> <i class="fa fa-plus-circle" aria-hidden="true"></i> New Company</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Profile</th>
            <th>Web Site</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($companies as $company)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $company->name }}</td>
            <td>{{ $company->email }}</td>
            <td><img style="margin-left:25px;border-radius: 50%;" src="/storage/app/public/{{$company->logo}}" width="40" height="40" /></td>
            <td>{{ $company->web_site }}</td>
            <td>
                <form action="{{ route('companies.destroy',$company->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('companies.show',$company->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
    
                    <a class="btn btn-primary" href="{{ route('companies.edit',$company->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $companies->links() !!}
      
@endsection