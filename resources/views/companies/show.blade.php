@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Company</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('companies.index') }}"> <i class="fa fa-chevron-left" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
   
    <div class="row" style="padding: 10px 50px 0px 50px;">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $company->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>E-mail:</strong>
                {{ $company->email }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Profile:</strong>
                <img style="margin-left:25px;border-radius: 50%;" src="/storage/app/public/{{$company->logo}}" width="100" height="100" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Web Site:</strong>
                {{ $company->web_site }}
            </div>
        </div>
    </div>
@endsection